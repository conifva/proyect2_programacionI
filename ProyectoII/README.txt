# Maze
Jugadores	1 jugador
Edades	 +6
Complejidad	Baja
Estrategia	Media
Habilidades	Estrategia abstracta

Maze es un juego de un laberinto sencillo formado artificiosamente por muros (bloques azules), encrucijadas y caminos sin salida para confundir al jugador en su busqueda por la meta.

# Obtención del juego
Clonar este repositorio y ejecutar "python maze.py" dentro de la carpeta "Proyect1".

# Prerrequisitos
- Python 3. Desarrollado y probado en 3.5.6.
- Módulo de programación gráfica Turtle
- Biblioteca gráfica Tkinter

# Objetivo
Guía a Kirby a traves del laberinto desde el punto de inicio hasta la meta marcado con una estrella. 
El juego no incluye la característica de temporizador, por lo que el jugador debe depender de su paciencia y precisión para llegar al otro extremo. 

# Controles
El laberinto se cotrola con teclas direccionales: flechas o las teclas A, S, D, W.
El menú del juego se controla a traves de opciones numéricas.

# Sobre el código
El juego se basa en dos codigos bastante diferentes entre si; por una parte esta "back_maze.py" que es el principal y el cual se ejectuta por terminal, que se fundamenta en el proyecto anterior alogado en el siguiente repositorio: 'https://github.com/covalenzuela/cvalenzuela_repoSoluciones/tree/master/Proyect1', aquí se explica en detalle su funcionamiento aunque cuenta con unas minimas modificaciones que se tratarán com mayor detalles en el otro archivo .py, "Laberinto.py".
El archivo "Laberinto.py" basicamente se encarga de resolver automaticamente 10 laberintos distintos, los que aparecerán al azar si se ejecuta la opcion N° 5. La logica detras de este codigo es la siguiente:
- La tortuga sólo puede pasar a través de los cuadrados abiertos del laberinto. Si se topa con una pared, debe intentar continuar en una dirección diferente. Por lo tanto, requerirá de un procedimiento sistemático para encontrar su salida del laberinto. Siguiendo el siguiente procedimiento:
    - Desde nuestra posición de partida, primero intentará avanzar una posición hacia el Norte y luego recursivamente probará nuestro procedimiento desde allí.
    - Si no tiene éxito al intentar un camino hacia el Norte como primer paso, dará un paso hacia el Sur y repetirá recursivamente nuestro procedimiento.
    - Si ir hacia el Sur no funciona, entonces intenta dar un paso hacia el Oeste como nuestro primer paso y aplica el procedimiento recursivamente.
    - Si el Norte, el Sur y el Oeste no han tenido éxito, aplica el procedimiento recursivamente desde una posición un paso hacia nuestro Este.
    - Si ninguna de estas direcciones funciona entonces no hay manera de salir del laberinto y hemos fracasado.
- Para evitar hacer el mismo recorrido una y otra vez, es decir, entrar en un ciclo infinito, se cuenta con una estrategia para recordar dónde hemos estado. Para explicar esto de la manera más lúdica posible podriamos hacer una analogia con dejar migas de pan en nuestro recorrido, por lo que si damos un paso en una dirección determinada y encontramos que ya hay una miga de pan en ese cuadrado (ya fue explorado), sabemos que debemos retroceder inmediatamente y probar la siguiente dirección en nuestro procedimiento. Retroceder es tan simple como regresar desde una llamada recursiva de una función. En este algoritmo hay cuatro casos base a considerar:
    - La tortuga se ha topado con una pared (Dado que el cuadrado está ocupado por una pared, no se puede realizar más exploración).
    - La tortuga ha encontrado un cuadrado que ya ha sido explorado. No se sigue explorando desde esta posición puesto que se entraría en un ciclo.
    - Se ha encontrado un borde exterior que no está ocupado por una pared. En otras palabras, hemos encontrado un camino para salir del laberinto.
    - Se ha explorado un cuadrado sin éxito en las cuatro direcciones.
- Para representar el laberinto se utiliza el módulo turtle para dibujar y explorar los laberintos de modo que se puede ver el algoritmo en acción. El objeto Laberinto proporcionará los siguientes métodos para que los usemos al escribir nuestro algoritmo de búsqueda:
    - "__init__" lee en un archivo de datos que representa un laberinto, inicializa la representación interna del laberinto y encuentra la posición inicial para la tortuga.
    - "_dibujarLaberinto_" dibuja el laberinto en una ventana en la pantalla.
    - "_actualizarPosicion_" actualiza la representación interna del laberinto y cambia la posición de la tortuga en la ventana.
    - "_esSalida_" comprueba si la posición actual es una salida del laberinto.
- La clase Laberinto también sobrecarga el operador índice [] para que el algoritmo pueda acceder fácilmente al estado de cualquier cuadrado particular.
- En cuanto a la función de búsqueda que denominamos "_buscarDesde_": Esta función toma tres parámetros: un objeto laberinto, la fila de inicio y la columna de inicio. Esto es importante porque, como función recursiva, la búsqueda comienza lógicamente otra vez con cada llamada recursiva. Es en esta funcion donde se analizan los anteriormente mencionados.
- Al momento de ejecutarse este código lo primero que se hace es llamar a "_actualizarPosicion_". Ésto se hace simplemente para visualizar cómo explora la tortuga su camino a través del laberinto. A continuación, se comprueban los tres primeros de los cuatro casos base: ¿La tortuga ha chocado contra una pared? ¿La tortuga ha regresado a un cuadrado ya explorado? ¿La tortuga ha encontrado una salida? En el caso de que ninguna de estas condiciones sea verdadera entonces se continua la búsqueda recursivamente.
- El método "__init__" toma el nombre de un archivo como su único parámetro. Este archivo es uno texto que representa un laberinto usando caracteres “+” para las paredes, espacios en blanco para los cuadrados abiertos (caminos) y la letra “S” para indicar la posición de inicio de la tortuga. Cada fila de la variable "_listalaberinto_" también es una lista. Esta lista secundaria contiene un carácter por cada cuadrado utilizando los caracteres descritos anteriormente.
- El método "_dibujarLaberinto_" utiliza esta representación interna para dibujar en la pantalla la vista inicial del laberinto.
- El método "_actualizarPosicion_", utiliza la misma representación interna para ver si la tortuga se ha encontrado con una pared. También actualiza la representación interna con un “.” o un “-” para indicar que la tortuga ha visitado un cuadrado particular o si el cuadrado es parte de un callejón sin salida. Además, este método utiliza dos métodos auxiliares, moverTortuga y tirarMigaDePan, para actualizar la vista en la pantalla.
- Finalmente, el método "_esSalida_" utiliza la posición actual de la tortuga para probar una condición de salida. ESta condición se da cuando la tortuga ha navegado hasta el borde del laberinto, ya sea la fila cero o la columna cero, o la columna de la derecha o la fila inferior.

# Errores del programa:
- Al momento de elegir una opción en el menú del juego se ha de evitar presionar cualquier otra tecla distinta de un numero, puesto que no las reconocerá y no funcionará correctamente. Se soluciona volviendo a ejecutar el programa e ingresando un parámetro válido.
- Luego de alcanzar la meta el jugador puede continuar moviendose por el laberinto.
- Desde el inicio se ve en el menú de la terminal la opción de recrear la ruta, la cual no es válida hasta luego de completar el laberinto, en caso de ejecutala antes de jugar esta mostrará un error y se tendra que volver a cargar el programa.
- Las opciones 1, 2 y 3 se pueden ejercutar sucesivamente sin problemas pero para ejecutar la opcion 5 de la manera correcta se debe de volver a iniciar el programa.

# Autor
Constanza Valenzuela
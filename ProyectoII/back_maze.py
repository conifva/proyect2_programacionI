# MAZE.PY
import turtle
import random
from Laberinto import Laberinto

PARTE_DEL_CAMINO = 'O'
INTENTADO = '.'
OBSTACULO = '+'
CAJELLON_SIN_SALIDA = '-'

#MURO = 'X'
#SIN_SALIDA = '-'
#CAMINO = 'O'
#INTENTADO = '.'


class maze():
	def __init__(self, automatico=None):
		# register shapes
		turtle.register_shape("star.gif")
		turtle.register_shape("kirby.gif")
		turtle.register_shape("wall3.gif")

		# create Pen
		class Pen(turtle.Turtle):
			def __init__(self):  # special comand
				turtle.Turtle.__init__(self)
				# self.shape("square")
				self.penup()
				self.speed(0)

		# create player object
		class Player(turtle.Turtle):
			def __init__(self):
				turtle.Turtle.__init__(self)
				self.shape("kirby.gif")
				self.pencolor("black")
				self.penup()
				self.speed(0)

			def marcar(self, color):
				self.dot(10, color)

			def position_player(self, x=0, y=0, condition=None):
				print(int((player.xcor() + 288) / 24), int((player.ycor() - 288) / -24))

				if condition:
					laberinto[int((player.xcor() + 288) / 24)][int((player.ycor() - 288) / -24)] = condition

				self.move(x, y)

				if condition == PARTE_DEL_CAMINO:
					print("BLANCO")
					color = "white"
				elif condition == OBSTACULO:
					print("AZUL")
					color = "blue"
				elif condition == CAJELLON_SIN_SALIDA:  # callejon sin salida
					print("ROJO")
					color = "red"
				elif condition == INTENTADO:  # intentado
					print("VERDE")
					color = "green"
				else:
					color = None

				if color:
					self.marcar(color)

			def buscarCamino(self, x=0, y=0):
				player.position_player(x, y)

				# Verificar casos base:
				# 1. Hemos tropezado con un muro, devolver False
				if laberinto[int((player.xcor() + 288) / 24)][int((player.ycor() - 288) / - 24)] == OBSTACULO:
					print("Encontro muro")
					return False

				# 2. Hemos encontrado un cuadrado que ya ha sido explorado
				if laberinto[int((player.xcor() + 288) / 24)][int((player.ycor() - 288) / - 24)] == INTENTADO:
					return False

				# 3. Exito, un borde exterior no ocupado por un obstaculo
				if player.collisions(end):
					player.position_player(x, y, PARTE_DEL_CAMINO)
					return True

				player.position_player(x, y, INTENTADO)

				# De lo contrario, use cortocircuitos logicos para probar cada
				# direccion a su vez (si fuera necesario)
				encontrado = player.buscarCamino(- 24, 0) or \
					player.buscarCamino(24, 0) or \
					player.buscarCamino(0, - 24) or \
					player.buscarCamino(0, 24)

				if encontrado:
					player.position_player(x, y, PARTE_DEL_CAMINO)
				else:
					player.position_player(x, y, CAJELLON_SIN_SALIDA)

				return encontrado

			def move(self, x, y):
				go_to_x = player.xcor() + x
				go_to_y = player.ycor() + y

				if (go_to_x, go_to_y) not in walls:
					self.goto(go_to_x, go_to_y)

			def move_up(self):
				go_to_x = player.xcor()
				go_to_y = player.ycor() + 24

				if (go_to_x, go_to_y) not in walls:
					self.goto(go_to_x, go_to_y)

			def move_down(self):
				go_to_x = player.xcor()
				go_to_y = player.ycor() - 24

				if (go_to_x, go_to_y) not in walls:
					self.goto(go_to_x, go_to_y)

			def move_left(self):
				go_to_x = player.xcor() - 24
				go_to_y = player.ycor()

				if (go_to_x, go_to_y) not in walls:
					self.goto(go_to_x, go_to_y)

			def move_right(self):
				go_to_x = player.xcor() + 24
				go_to_y = player.ycor()

				if (go_to_x, go_to_y) not in walls:
					self.goto(go_to_x, go_to_y)

			# collisions
			def collisions(self, other):
				if (self.xcor(), self.ycor()) in other:
					print("GANASTE")
					return True
				else:
					return False

		#wn=window
		wn = turtle.Screen()
		wn.clear()
		wn.bgcolor("black")
		wn.title("Juego del Laberinto")
		wn.screensize(canvwidth=700, canvheight=700, bg=None)

		self.window = wn

		# define level 1
		level_1 = [
			"XXXXXXXXXXXXXXXXXXXXXXXXX",
			"XP XXXXXXX          XXXXX",
			"X  XXXXXXX  XXXXXX  XXXXX",
			"X       XX  XXXXXX  XXXXX",
			"X       XX  XXX        XX",
			"XXXXXX  XX  XXX        XX",
			"XXXXXX  XX  XXXXXX  XXXXX",
			"XXXXXX  XX    XXXX  XXXXX",
			"X  XXX        XXXX  XXXXX",
			"X  XXX  XXXXXXXXXXXXXXXXX",
			"X         XXXXXXXXXXXXXXX",
			"X                XXXXXXXX",
			"XXXXXXXXXXXX     XXXXX  X",
			"XXXXXXXXXXXXXXX  XXXXX  X",
			"XXX  XXXXXXXXXX         X",
			"XXX                     X",
			"XXX         XXXXXXXXXXXXX",
			"XXXXXXXXXX  XXXXXXXXXXXXX",
			"XXXXXXXXXX              X",
			"XX E XXXXX              X",
			"XX   XXXXXXXXXXXXX  XXXXX",
			"XX    XXXXXXXXXXXX  XXXXX",
			"XX          XXXX        X",
			"XXXXX                   X",
			"XXXXXXXXXXXXXXXXXXXXXXXXX"
		]

		# add a end list
		end = []

		# create level setup Function
		def start_up_maze(level):
			for y in range(len(level)):
				for x in range(len(level[y])):
					# get the character at each x,y coordinate
					# NOTE the order of y and x in the next line
					character = level[y][x]
					# calculate the screen x, y coordinates
					screen_x = -288 + (x * 24)
					screen_y = 288 - (y * 24)

					# check if is X (wall)
					if character == "X":
						pencil.goto(screen_x, screen_y)
						pencil.shape("wall3.gif")
						pencil.stamp()
						walls.append((screen_x, screen_y))

					# check if is a P (player)
					if character == "P":
						player.goto(screen_x, screen_y)

					# check if is a E (end)
					if character == "E":
						pencil.shape("star.gif")
						pencil.goto(screen_x, screen_y)
						pencil.stamp()
						end.append((screen_x, screen_y))

		# create class instances
		pencil = Pen()
		player = Player()

		walls = []
		#recorrido = []
		laberinto = []

		for x in range(len(level_1)):
			laberinto.append([])
			for y in range(len(level_1[x])):
				laberinto[x].append(level_1[x][y])

		# Start up level
		if automatico is None:
			start_up_maze(laberinto)
			player.pd()
			#print(walls)

			# Controls
			turtle.listen()
			turtle.onkey(player.move_left, "Left")
			turtle.onkey(player.move_right, "Right")
			turtle.onkey(player.move_down, "Down")
			turtle.onkey(player.move_up, "Up")

			# or
			turtle.onkey(player.move_left, "a")
			turtle.onkey(player.move_right, "d")
			turtle.onkey(player.move_down, "s")
			turtle.onkey(player.move_up, "w")

			# Main Game Loop
			while True:
				# update screen
				wn.update()
				# verify for player collision with end (call collisions)
				if player.collisions(end):
					break

		else:
			start_up_maze(laberinto)
			player.pd()
			player.position_player()
			player.buscarCamino()
			#print(walls)
			#position_player()

	def get_window(self):
		return self.window


def buscarDesde(laberinto, filaInicio, columnaInicio):
	laberinto.actualizarPosicion(filaInicio, columnaInicio)
	# Verificar casos base:
	# 1. Hemos tropezado con un obstaculo, devolver False
	if laberinto[filaInicio][columnaInicio] == OBSTACULO:
		return False
	# 2. Hemos encontrado un cuadrado que ya ha sido explorado
	if laberinto[filaInicio][columnaInicio] == INTENTADO:
		return False
	# 3. Exito, un borde exterior no ocupado por un obstaculo
	if laberinto.esSalida(filaInicio, columnaInicio):
		laberinto.actualizarPosicion(filaInicio, columnaInicio, PARTE_DEL_CAMINO)
		return True

	laberinto.actualizarPosicion(filaInicio, columnaInicio, INTENTADO)

	# De lo contrario, use cortocircuitos logicos para probar cada
	# direccion a su vez (si fuera necesario)
	encontrado = buscarDesde(laberinto, filaInicio - 1, columnaInicio) or \
				buscarDesde(laberinto, filaInicio + 1, columnaInicio) or \
				buscarDesde(laberinto, filaInicio, columnaInicio - 1) or \
				buscarDesde(laberinto, filaInicio, columnaInicio + 1)

	if encontrado:
		laberinto.actualizarPosicion(filaInicio, columnaInicio, PARTE_DEL_CAMINO)
	else:
		laberinto.actualizarPosicion(filaInicio, columnaInicio, CAJELLON_SIN_SALIDA)

	return encontrado


# Menu
while True:
	print("Elija una opcion:")
	print("(1) Jugar")
	print("(2) Ruta")
	print("(3) Instrucciones")
	print("(4) Salir")
	print("(5) Jugar automatico")

	option = int(input())
	if option == 1:
		game = maze()
	elif option == 2:
		window = game.get_window()
		window.bgcolor("grey")
	elif option == 3:
		print("Ayuda a Kirby a llegar a la estrella.")
		print("Para desplazarte puedes utilizar las teclas direcionales.")
	elif option == 4:
		print("Bye bye")
		break
	elif option == 5:
		#game = maze(True)
		Lista = ['laberinto1.txt',
				'laberinto2.txt',
				'laberinto3.txt',
				'laberinto4.txt',
				'laberinto5.txt',
				'laberinto6.txt',
				'laberinto7.txt',
				'laberinto8.txt',
				'laberinto9.txt',
				'laberinto10.txt']

		miLaberinto = Laberinto(Lista[random.randint(0, 9)])
		miLaberinto.dibujarLaberinto()
		miLaberinto.actualizarPosicion(miLaberinto.filaInicio, miLaberinto.columnaInicio)
		buscarDesde(miLaberinto, miLaberinto.filaInicio, miLaberinto.columnaInicio)
